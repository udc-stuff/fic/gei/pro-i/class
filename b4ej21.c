#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

const int MAX_DESC = 1000;
const int MAX_TASK_PER_DAY = 20;
struct time {
	int hour, minute, second;
};
struct date {
	int day, month, year;
};
struct task {
	char desc[MAX_DESC];
	struct time time;
};
struct schedule {
	struct date date;
	struct task tasks[MAX_TASK_PER_DAY];
	int amount;
};

int count_char(char *s, char c) {
	int cnt, amount;
	for (amount = cnt = 0; s[cnt] != '\0'; ++cnt)
		if (s[cnt] == c)
			++amount;

	return amount; 
}

bool parse_int(char *s, int *n) {
	int cnt;
	for (*n = cnt = 0; s[cnt] != '\0'; ++cnt) {
		if (s[cnt] >= '0' && s[cnt] <= '9') {
			*n = (*n) * 10 + (s[cnt] - '0');
		} else {
			return false;
		}
	}

	return true;
}

bool parse_time(char *s, struct time *t) {
	char *ptr, *tmp;
	if (count_char(s, ':') != 2) {
		printf("Formato incorrecto\n");
		return false;
	}

	ptr = strchr(s, ':');
	*ptr = '\0';
	if (!parse_int(s, &t->hour)) {
		printf("Formato incorrecto\n");
		return false;
	}

	tmp = ptr + 1;
	ptr = strchr(tmp, ':');
	if (!parse_int(s, &t->minute)) {
		printf("Formato incorrecto\n");
		return false;
	}

	if (!parse_int(ptr + 1, &t->second)) {
		printf("Formato incorrecto\n");
		return false;
	}

	return true;
}

bool parse_date(char *s, struct date *d) {
	char *ptr, *tmp;
	if (count_char(s, '/') != 2 ) {
		printf("Formato incorrecto\n");
		return false;
	}

	ptr = strchr(s, '/');
	*ptr = '\0';
	if (!parse_int(s, &d->day)) {
		printf("Formato incorrecto\n");
		return false;
	}

	tmp = ptr + 1;
	ptr = strchr(tmp, '/');
	*ptr = '\0';
	if (!parse_int(tmp, &d->month)) {
		printf("Formato incorrecto\n");
		return false;
	}
	if (!parse_int(ptr+1, &d->year)) {
		printf("Formato incorrecto\n");
                return false;
	}
	
	return true;
}

bool validate_time(struct time t) {
	if (t.hour < 0 || t.hour > 23)
		return false;

	if (t.minute < 0 || t.minute > 59)
		return false;

	if (t.second < 0 || t. second > 59)
		return false;

	return true;
}

bool validate_date(struct date d) {
	if (d.day <= 0 || d.month <= 0 || d.year <= 0)
		return false;

	if (d.month > 12)
		return false;

	switch (d.month) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			return d.day <= 31;
		case 2:
			if (d.year % 4 == 0 && (d.year % 100 != 0 || d.year % 400 == 0))
				return d.day <= 29;
			else
				return d.day <= 28;
		case 4:
		case 6:
		case 9:
		case 11:
			return d.day <= 30;
	}
	return false;
}

struct time get_time() {
	struct time tmp;
	const int MAX = 10;
	bool right = false;
	char aux[MAX];
	char *ptr;

	while (!right) {
		printf("Introduce la hora (hh:mm:ss): ");
		fgets(aux, MAX, stdin);
		if ((ptr = strchr(aux, '\n')) == NULL ) {
			printf("Formato incorrecto\n");
		} else {
			*ptr = '\0';
			right = parse_time(aux, &tmp);
                        if (right) {
                                right = validate_time(tmp);
                                if (!right)
                                        printf("Formato incorrecto\n");
                        }
		}
	}
}

struct date get_date() {
	struct date tmp;
	const int MAX = 12;
	bool right = false;
	char aux[MAX];
	char *ptr;

	while (!right) {
		printf("Introduce la fecha (dd/mm/yyyy): ");
		fgets(aux, MAX, stdin);
		if ((ptr = strchr(aux, '\n')) == NULL) {
			printf("Formato incorrecto\n");
		} else {
			*ptr = '\0';
			right = parse_date(aux, &tmp);
			if (right) {
				right = validate_date(tmp);
				if (!right)
					printf("Formato incorrecto\n");
			}
		}
	}
	return tmp;
}

struct date inc_date(struct date d) {
	struct date tmp = d;
	tmp.day++;

	if (validate_date(tmp))
		return tmp;

	tmp.day = 1;
	tmp.month++;
	if (validate_date(tmp))
		return tmp;

	tmp.month = 1;
	tmp.year++;

	return tmp;
}

void print_schedule(struct schedule *s, int size) {
	int cnt;

	for (cnt = 0; cnt < size; ++cnt) {
		printf("DATE: %2d/%02d/%d\n", s[cnt].date.day, s[cnt].date.month, s[cnt].date.year);
	}
}

bool get_yes_no(char *s) {
	const int SIZE = 10;
	char buff[SIZE];

	while (true) {
		printf("%s", s);
		fget(buff, SIZE, stdin);
		if (strcmp(buff, "y\n") == 0)
			return true;
		if (strcmp(buff, "n\n") == 0)
			return false;
	}

}

void get_tasks(struct schedule *schedule, int max_tasks) {
	int cnt;

	printf("Introducir las tareas del dia %d/%d/%d\n", schedule->date.day, schedule->date.month, schedule->date.year);

	for (cnt = 0; cnt < max_tasks; ++cnt) {
		printf("Introduce una descripcion: ");
		fgets(schedule->tasks[cnt].desc, MAX_DESC, stdin);		

		schedule->tasks[cnt].time = get_time();

		if (cnt + 1 < max_tasks) 
			if (!get_yes_no("Quieres continuar?"))
				break;
	}

	schedule->amount = cnt;
}

int main() {
	const int MAX_SCHEDULE = 7;
	struct schedule schedules[MAX_SCHEDULE];
	int cnt;

	struct date date = get_date();
	printf("%d/%d/%d\n", date.day, date.month, date.year);

	schedules[0].date = date;
	for (cnt = 0; cnt < MAX_SCHEDULE; ++cnt) {
		if (cnt != 0)
			schedules[cnt].date = inc_date(schedules[cnt-1].date);

		get_tasks(&schedules[cnt], MAX_TASK_PER_DAY);
	}

	print_schedule(schedules, MAX_SCHEDULE);




	return 0;
}
