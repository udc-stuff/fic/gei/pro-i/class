#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int int_cmp(const void *e1, const void *e2) {
	int *a = (int *) e1;
	int *b = (int *) e2;

	return *a - *b;
}

int main() {
	const int MAX = 23;
	int a[MAX];
	int cnt;
	srand(time(NULL));
	for (cnt = 0; cnt < MAX; ++cnt) {
		a[cnt] = rand() % (MAX * 2);
		printf("%s%d%s", (cnt == 0)?"[":" ", a[cnt], (cnt + 1 == MAX)? "]\n": ",");
	}
	printf("\n");
	
	// ordenarlo
	qsort(a, MAX, sizeof(a[0]), int_cmp);

	//mostrarlo
	for (cnt = 0; cnt < MAX; ++cnt)
		printf("%s%d%s", (cnt == 0)?"[":" ", a[cnt], (cnt + 1 == MAX)? "]\n": ",");
        printf("\n");

	return 0;
}
