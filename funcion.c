#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

bool pequeno(int a, int b) {
	return a < b;
}

bool mayor(int a, int b) {
	return a > b;
}

int foo(int *a, int size, bool (* cmp)(int , int ) ) {
	int cnt;
	int aux;
	for (aux = cnt = 0; cnt < size; ++cnt)
		if (!cmp(a[aux],a[cnt]))
			aux = cnt;

	return aux;
}
int main() {
	const int MAX = 10;
	int a[MAX];
	int cnt;
	srand(0);
	for (cnt = 0; cnt < MAX; ++cnt) {
		a[cnt] = rand() % (MAX * 2);
		printf("%s%d%s", (cnt == 0)?"[":" ", a[cnt], (cnt + 1 == MAX)? "]\n": ",");
	}
	printf("\n");

	cnt = foo(a, MAX, pequeno);
	printf("minimo: [%d]= %d\n", cnt, a[cnt]);
	
	cnt = foo(a, MAX, mayor);
	printf("maximo: [%d]= %d\n", cnt, a[cnt]);
	

}
